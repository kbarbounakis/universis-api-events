import {ApplicationService} from "@themost/common";

declare class EventService extends ApplicationService {
    install();
    uninstall();
}

export {
    EventService
}
